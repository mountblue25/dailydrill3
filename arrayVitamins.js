const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];

// Problem1

let fruits = items.map((object) => {
    return object.name
})
console.log(fruits)

// Problem2

let fruitsContainCvitamin = items.filter((object) => {
    return object.contains.localeCompare("Vitamin C") === 0 && object.available === true
}).map((object) => {
    return object.name
})
console.log(fruitsContainCvitamin)

// Problem3

let fruitsContainAvitamin = items.filter((object) => {
    return object.contains.includes("Vitamin A") && object.available === true
}).map((object) => {
    return object.name
})
console.log(fruitsContainAvitamin)

// Problem4

let groupedItems = items.reduce((accumelator, current) => {
    if (current.available === true){
        let vitamins = current.contains.split(", ")
        
        if (vitamins.length > 1){
            let result = vitamins.map((vit) => {
                if(accumelator.hasOwnProperty(vit)){
                    accumelator[vit].push(current.name)
                }
                else{
                    accumelator[vit] = []
                    accumelator[vit].push(current.name)
                }
            })
        }
        else{
            if (accumelator.hasOwnProperty(...current.contains)){
                accumelator[current.contains].push(current.name)
            }
            else{
                accumelator[current.contains] = []
                accumelator[current.contains].push(current.name)
            }
            
        }
    }
    return accumelator

}, {})
console.log(groupedItems)

// Problem5

let result = items.sort((obj1, obj2) => {
    if (obj1.contains.length > obj2.contains.length){
            return 1
        }
    else{
        return -1
    }
})
console.log(result)


